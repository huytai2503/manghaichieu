package khtn.models;

public interface InModels {
	public String[][] veChuA();
	public String[][] veChuB();
	public String[][] veChuC();
	public String[][] veChuD();
	public String[][] veChuE();
	public String[][] veChuF();
	public String[][] veChuG();
	public String[][] veChuH();
	public String[][] veChuI();
	public String[][] veChuJ();
	public String[][] veChuK();
	public String[][] veChuL();
	public String[][] veChuM();
	public String[][] veChuN();
	public String[][] veChuO();
	public String[][] veChuP();
	public String[][] veChuQ();
	public String[][] veChuR();
	public String[][] veChuS();
	public String[][] veChuT();
	public String[][] veChuU();
	public String[][] veChuV();
	public String[][] veChuW();
	public String[][] veChuX();
	public String[][] veChuY();
	public String[][] veChuZ();
	public String[][] veTamGiacBtmL();
	public String[][] veTamGiacBtmR();
	public String[][] veTamGiacUpL();
	public String[][] veTamGiacUpR();
	public String[][] veTamGiacUp();
	public String[][] veTamGiacDown();
	public String[][] veTamGiacLeft();
	public String[][] veTamGiacRight();
}
