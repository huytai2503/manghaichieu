package khtn.models;

public class MangHaiChieu extends AbCreatXY implements InModels{
	String arr[][] = new String[trucX][trucY];
	
	public MangHaiChieu(int trucX, int trucY) {
		super(trucX, trucY);
	}
	public MangHaiChieu(int trucX, int trucY, String[][] arr) {
		super(trucX, trucY);
		this.arr = arr;
	}
	public String[][] getArr() {
		return arr;
	}
	public void setArr(String[][] arr) {
		this.arr = arr;
	}
	public int size() {
		return this.arr.length;
	}
	@Override
	public String[][] veChuA() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==4-X) arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(Y==4+X) arr[X][Y]="*";
				if(X==2&&Y>2&&Y<6) arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuB() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if((Y==0||Y==8)&&(X!=0&&X!=8&&X!=4))arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(Y<7&&(X==0||X==8))arr[X][Y]="*";
				if(Y<8&&X==4)arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuC() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==0&&(X>1&&X<7))arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(Y==8&&(X==2||X==6))arr[X][Y]="*";
				if((Y==1||Y==7)&&(X==1||X==7))arr[X][Y]="*";
				if((Y>1&&Y<7)&&(X==0||X==8))arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuD() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==0||(Y==8&&X>1&&X<7))arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(Y==7&&(X==1||X==7))arr[X][Y]="*";
				if(Y<6&&(X==0||X==8))arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuE() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==0)arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(X==0||X==4||X==8)arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuF() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==0)arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(X==0||X==4)arr[X][Y]="*";
			}
		}return arr;	}
	@Override
	public String[][] veChuG() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==0&&(X>1&&X<7))arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(Y==8&&(X==2||X==6))arr[X][Y]="*";
				if((Y==7||Y==1)&&(X==1||X==7))arr[X][Y]="*";
				if((Y>1&&Y<7)&&(X==0||X==8))arr[X][Y]="*";
				if(Y>4&&X==5)arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuH() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==0||Y==8)arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(X==4)arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuI() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==7)arr[X][Y]="*";
				else arr[X][Y]=" ";
			}
		}return arr;
	}
	@Override
	public String[][] veChuJ() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==7&&X<8)arr[X][Y]="*";
				else arr[X][Y]=" ";
				if((Y>5&&Y<9)&&X==0)arr[X][Y]="*";
				if((Y>3&&Y<7)&&X==8)arr[X][Y]="*";
				if(Y==3&&X==7)arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuK() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==0)arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(Y<6&&(Y==5-X))arr[X][Y]="*";
				if(Y==X-3&&X>4)arr[X][Y]="*";
			}
		}return arr;	
	}
	@Override
	public String[][] veChuL() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if(Y==0)arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(X==8)arr[X][Y]="*";
			}
		}return arr;	
	}
	@Override
	public String[][] veChuM() {
		int Y=0,X=0;
		for(Y=0;Y<trucX;Y++){
			for(X=0;X<trucY;X++){
				if(Y==0||Y==8) arr[X][Y]="*";
				else arr[X][Y]=" ";
				if((Y==X||Y==8-X)&&X<5) arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuN() {
		int Y=0,X=0;
		for(Y=0;Y<trucX;Y++){
			for(X=0;X<trucY;X++){
				if(Y==0||Y==8) arr[X][Y]="*";
				else arr[X][Y]=" ";
				if(Y==X) arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuO() {
		int X=0,Y=0;
		for(X=0;X<trucX;X++){
			for(Y=0;Y<trucY;Y++){
				if((Y==0||Y==8)&&(X>1&&X<7))arr[X][Y]="*";
				else arr[X][Y]=" ";
				if((Y==1||Y==7)&&(X==1||X==7))arr[X][Y]="*";
				if((Y>1&&Y<7)&&(X==0||X==8))arr[X][Y]="*";
			}
		}return arr;
	}
	@Override
	public String[][] veChuP() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuQ() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuR() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuS() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuT() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuU() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuV() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuW() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuX() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuY() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veChuZ() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veTamGiacBtmL() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veTamGiacBtmR() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veTamGiacUpL() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veTamGiacUpR() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veTamGiacUp() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veTamGiacDown() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veTamGiacLeft() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[][] veTamGiacRight() {
		// TODO Auto-generated method stub
		return null;
	}
}
