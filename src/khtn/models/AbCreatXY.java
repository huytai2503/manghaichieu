package khtn.models;

public abstract class AbCreatXY {
	int trucX;
	int trucY;
	public AbCreatXY(int trucX, int trucY) {
		super();
		this.trucX = trucX;
		this.trucY = trucY;
	}
	public int getTrucX() {
		return trucX;
	}
	public void setTrucX(int trucX) {
		this.trucX = trucX;
	}
	public int getTrucY() {
		return trucY;
	}
	public void setTrucY(int trucY) {
		this.trucY = trucY;
	}
}
